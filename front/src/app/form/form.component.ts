import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-form",
  templateUrl: "./form.component.html",
  styleUrls: ["./form.component.scss"],
})
export class FormComponent implements OnInit {
  texts = ["one", "two"];
  button = {
    disabled: true,
  };
  constructor() {}

  ngOnInit(): void {
    setTimeout(() => {
      this.button.disabled = false;
    }, 3000);
  }
}
