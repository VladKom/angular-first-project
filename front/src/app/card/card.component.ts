import { Component, Input, OnInit } from "@angular/core";
import { Card } from "../app.component";

@Component({
  selector: "app-card",
  templateUrl: "./card.component.html",
  styleUrls: ["./card.component.scss"],
})
export class CardComponent implements OnInit {
  @Input() card: Card;
  @Input() index: number;

  ngOnInit() {
    setTimeout(() => {
      this.urlLogo = this.urlLogoBlack;
    }, 3000);
  }
  changeTitle() {
    this.card.title = "NEW Title";
  }
  inputHandler(value) {
    this.card.title = value;
  }
  changeHandler() {
    console.log(this.card.title);
  }

  textColor: string;
  cardDate: Date = new Date();

  urlLogoBlack: string =
    "https://cdn4.iconfinder.com/data/icons/logos-and-brands-1/512/21_Angular_logo_logos-512.png";
  urlLogo: string = "https://cdn.worldvectorlogo.com/logos/angular-icon.svg";
  obj = { name: "Vlad", age: "17", work: "Frontend" };
  getInfo() {
    return "recived data from method";
  }
}
